<?php

namespace AppBundle\Entity;

/**
 * Consumer
 */
class Consumer
{
    /**
     * @var mixed
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $address;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return Consumer
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }
}

