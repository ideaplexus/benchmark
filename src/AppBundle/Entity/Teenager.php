<?php

namespace AppBundle\Entity;

/**
 * Teenager
 */
class Teenager
{
    /**
     * @var mixed
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $friendsWithMe;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $myFriends;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->friendsWithMe = new \Doctrine\Common\Collections\ArrayCollection();
        $this->myFriends = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add friendsWithMe
     *
     * @param \AppBundle\Entity\Teenager $friendsWithMe
     *
     * @return Teenager
     */
    public function addFriendsWithMe(\AppBundle\Entity\Teenager $friendsWithMe)
    {
        $this->friendsWithMe[] = $friendsWithMe;

        return $this;
    }

    /**
     * Remove friendsWithMe
     *
     * @param \AppBundle\Entity\Teenager $friendsWithMe
     */
    public function removeFriendsWithMe(\AppBundle\Entity\Teenager $friendsWithMe)
    {
        $this->friendsWithMe->removeElement($friendsWithMe);
    }

    /**
     * Get friendsWithMe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFriendsWithMe()
    {
        return $this->friendsWithMe;
    }

    /**
     * Add myFriend
     *
     * @param \AppBundle\Entity\Teenager $myFriend
     *
     * @return Teenager
     */
    public function addMyFriend(\AppBundle\Entity\Teenager $myFriend)
    {
        $this->myFriends[] = $myFriend;

        return $this;
    }

    /**
     * Remove myFriend
     *
     * @param \AppBundle\Entity\Teenager $myFriend
     */
    public function removeMyFriend(\AppBundle\Entity\Teenager $myFriend)
    {
        $this->myFriends->removeElement($myFriend);
    }

    /**
     * Get myFriends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyFriends()
    {
        return $this->myFriends;
    }
}

