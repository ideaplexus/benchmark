<?php

namespace AppBundle\Entity;

/**
 * Address
 */
class Address
{
    /**
     * @var mixed
     */
    private $id;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}

