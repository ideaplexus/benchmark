<?php

namespace AppBundle\Entity;

/**
 * Student
 */
class Student
{
    /**
     * @var mixed
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Student
     */
    private $mentor;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mentor
     *
     * @param \AppBundle\Entity\Student $mentor
     *
     * @return Student
     */
    public function setMentor(\AppBundle\Entity\Student $mentor = null)
    {
        $this->mentor = $mentor;

        return $this;
    }

    /**
     * Get mentor
     *
     * @return \AppBundle\Entity\Student
     */
    public function getMentor()
    {
        return $this->mentor;
    }
}

