<?php

namespace AppBundle\Entity;

/**
 * Role
 */
class Role
{
    /**
     * @var mixed
     */
    private $id;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}

