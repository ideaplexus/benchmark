<?php

namespace AppBundle\Entity;

/**
 * Shipment
 */
class Shipment
{
    /**
     * @var mixed
     */
    private $id;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}

