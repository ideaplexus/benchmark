<?php

namespace AppBundle\Entity;

/**
 * Customer
 */
class Customer
{
    /**
     * @var mixed
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Cart
     */
    private $cart;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cart
     *
     * @param \AppBundle\Entity\Cart $cart
     *
     * @return Customer
     */
    public function setCart(\AppBundle\Entity\Cart $cart = null)
    {
        $this->cart = $cart;

        if ($this->cart->getCustomer() !== $this) {
            $this->cart->setCustomer($this);
        }

        return $this;
    }

    /**
     * Get cart
     *
     * @return \AppBundle\Entity\Cart
     */
    public function getCart()
    {
        return $this->cart;
    }
}

