<?php

namespace AppBundle\Entity;

/**
 * Feature
 */
class Feature
{
    /**
     * @var mixed
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Application
     */
    private $application;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set application
     *
     * @param \AppBundle\Entity\Application $application
     *
     * @return Feature
     */
    public function setApplication(\AppBundle\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \AppBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }
}

