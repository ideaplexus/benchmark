<?php

namespace AppBundle\Entity;

/**
 * Product
 */
class Product
{
    /**
     * @var mixed
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Shipment
     */
    private $shipment;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shipment
     *
     * @param \AppBundle\Entity\Shipment $shipment
     *
     * @return Product
     */
    public function setShipment(\AppBundle\Entity\Shipment $shipment = null)
    {
        $this->shipment = $shipment;

        return $this;
    }

    /**
     * Get shipment
     *
     * @return \AppBundle\Entity\Shipment
     */
    public function getShipment()
    {
        return $this->shipment;
    }
}

