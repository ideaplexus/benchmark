<?php

namespace AppBundle\Console\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class AnalyseDatabaseCSVCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            //->setName('analyse:csv:database')
            ->setName('benchmark:analyse')
            ->addOption(
                'id-type',
                null,
                InputOption::VALUE_REQUIRED
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logDir = $this->getContainer()->get('kernel')->getLogDir();
        $idType = $input->getOption('id-type');
        $finder = new Finder();
        $finder->in($logDir)->files()->name('/.*ID_TYPE__' . $idType .'.*\.csv/');

        $results = [];
        foreach ($finder as $fileInfo) {
            $results = array_merge($results, $this->readFile($fileInfo));
        }

        $this->summarize($results, $output);

    }

    protected function summarize(array $results, OutputInterface $output)
    {
        $table = new Table($output);
        $table->setHeaders([
            'test',
            'average duration (sec)'
        ]);

        $summarize = [];
        foreach ($results as $result) {
            $row = array_values($result);
            $summarize[$row[0]] += (float) str_replace(',', '.', $row[2]);
        }

        foreach ($summarize as $key => $value) {
            $table->addRow([$key, number_format($value/count($results), 3, ',', '')]);
        }

        $table->render();
    }


    /**
     * @param SplFileInfo $fileInfo
     *
     * @return array
     */
    protected function readFile(SplFileInfo $fileInfo)
    {
        $serializer = $this->getContainer()->get('serializer');

        return $serializer->decode(file_get_contents($fileInfo->getPathname()), 'csv');
    }
}