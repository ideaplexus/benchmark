<?php

namespace AppBundle\Console\Command;

use AppBundle\Benchmark\Database;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class BenchmarkDatabaseCommand
 *
 * @package AppBundle\Console\Command
 */
class BenchmarkDatabaseCommand extends ContainerAwareCommand
{
    protected const QUIET                      = 'quiet';
    protected const SKIP_DROP_SCHEMA           = 'skip-drop-schema';
    protected const SKIP_CREATE_SCHEMA         = 'skip-create-schema';
    protected const SKIP_BENCHMARK             = 'skip-benchmark';
    protected const SKIP_DROP_SCHEMA_MESSAGE   = 'Skip dropping schema before the benchmark';
    protected const SKIP_CREATE_SCHEMA_MESSAGE = 'Skip create schema before the benchmark';
    protected const SKIP_BENCHMARK_MESSAGE     = 'Skip the benchmark';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        ini_set('memory_limit', '-1');

        $this
            ->setName('benchmark:database')
            ->addOption(
                self::SKIP_DROP_SCHEMA,
                null,
                InputOption::VALUE_NONE,
                self::SKIP_DROP_SCHEMA_MESSAGE
            )
            ->addOption(
                self::SKIP_CREATE_SCHEMA,
                null,
                InputOption::VALUE_NONE,
                self::SKIP_CREATE_SCHEMA_MESSAGE
            )
            ->addOption(
                self::SKIP_BENCHMARK,
                null,
                InputOption::VALUE_NONE,
                self::SKIP_BENCHMARK_MESSAGE
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stopwatch = new Stopwatch();

        $this
            ->dropSchema($input, $output)
            ->createSchema($input, $output)
            ->renderParameter($input, $output)
            ->executeBenchmark($input, $output, $stopwatch)
            ->renderResult($input, $output, $stopwatch)
            ->saveCSV($input, $output, $stopwatch)
        ;

        return 0;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     *
     * @throws \RuntimeException
     */
    protected function dropSchema(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption(self::SKIP_DROP_SCHEMA)) {
            $dropCommand   = $this->getApplication()->find('doctrine:schema:drop');
            $dropInput     = new ArrayInput([
                'command'          => 'doctrine:schema:drop',
                '--full-database'  => true,
                '--force'          => true,
                '--no-interaction' => true,
                '--quiet'          => $input->getOption(self::QUIET)
            ]);

            if (0 !== $dropCommand->run($dropInput, $output)) {
                throw new \RuntimeException('Database schema could not be dropped!');
            }
        } // no else

        return $this;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     *
     * @throws \RuntimeException
     */
    protected function createSchema(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption(self::SKIP_CREATE_SCHEMA)) {
            $dropCommand   = $this->getApplication()->find('doctrine:schema:create');
            $dropInput     = new ArrayInput([
                'command'          => 'doctrine:schema:create',
                '--no-interaction' => true,
                '--quiet'          => $input->getOption(self::QUIET)
            ]);

            if (0 !== $dropCommand->run($dropInput, $output)) {
                throw new \RuntimeException('Database schema could not be created!');
            }
        } // no else

        return $this;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function executeBenchmark(InputInterface $input, OutputInterface $output, Stopwatch $stopwatch)
    {
        if (!$input->getOption(self::SKIP_BENCHMARK)) {
            $options   = [
                'iterationSize' => getenv('ITERATION_SIZE') ?: Database::ITERATION_SIZE,
                'chunkSize'     => getenv('CHUNK_SIZE')     ?: Database::CHUNK_SIZE,
                'clearOnFlush'  => getenv('CLEAR_ON_FLUSH') ?: Database::CLEAR_ON_FLUSH,
            ];

            $manager   = $this->getContainer()->get('doctrine')->getManager();
            $benchmark = new Database($manager, $output, $stopwatch, $options);

            $benchmark->benchmarkAll();
        } // no else

        return $this;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return $this
     */
    protected function renderParameter(InputInterface$input, OutputInterface $output)
    {
        if (!$input->getOption(self::QUIET)) {
            $output->writeln('');
            $output->writeln('Benchmark parameter:');
            $table = new Table($output);

            foreach ($this->getParameterData() as $row) {
                $table->addRow($row);
            }

            $table->render();
        } // no else

        return $this;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param Stopwatch       $stopwatch
     *
     * @return $this
     */
    protected function renderResult(InputInterface $input, OutputInterface $output, Stopwatch $stopwatch)
    {
        if (!$input->getOption(self::SKIP_BENCHMARK) && !$input->getOption(self::QUIET)) {
            $output->writeln('');
            $output->writeln('Benchmark result:');
            $table = new Table($output);

            $stopwatchData = $this->getStopwatchData($stopwatch);
            $table->setHeaders(array_keys($stopwatchData[0]));

            foreach ($this->getStopwatchData($stopwatch) as $row) {
                $table->addRow($row);
            }

            $table->render();
        }

        return $this;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param Stopwatch       $stopwatch
     *
     * @return $this
     */
    protected function saveCSV(InputInterface $input, OutputInterface $output, Stopwatch $stopwatch)
    {
        if (!$input->getOption(self::SKIP_BENCHMARK)) {
            $parameterData = $this->getParameterData();
            $stopwatchData = $this->getStopwatchData($stopwatch);

            $filename = microtime(true);

            foreach ($parameterData as $row) {
                foreach ($row as $item) {
                    $filename .= '__' . $item;
                }
            }

            $filename = $this->getContainer()->get('kernel')->getLogDir() . '/' . $filename . '.csv';

            file_put_contents(
                $filename,
                $this->getContainer()->get('serializer')->encode($stopwatchData, 'csv')
            );

            if (!$input->getOption(self::QUIET))
            {
                $output->writeln('');
                $output->writeln('Result stored in ' . $filename);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getParameterData()
    {
        return [
            ['ID_TYPE', getenv('ID_TYPE') ?: $this->getContainer()->getParameter('id_type')],
            ['DATABASE_DRIVER', getenv('DATABASE_DRIVER')],
            ['DATABASE_HOST', getenv('DATABASE_HOST')],
            ['ITERATION_SIZE', getenv('ITERATION_SIZE') ?: Database::ITERATION_SIZE],
            ['CHUNK_SIZE', getenv('CHUNK_SIZE') ?: Database::CHUNK_SIZE],
            ['CLEAR_ON_FLUSH', getenv('CLEAR_ON_FLUSH') ?: Database::CLEAR_ON_FLUSH],
        ];
    }

    /**
     * @param Stopwatch $stopwatch
     *
     * @return array
     */
    protected function getStopwatchData(Stopwatch $stopwatch)
    {
        $data = [];
        foreach ($stopwatch->getSections() as $section) {
            foreach ($section->getEvents() as $name => $event) {
                foreach ($event->getPeriods() as $period) {
                    $data[] = [
                        'test'                  => $name,
                        'duration (sec)'        => number_format((float) $event->getDuration() / 1000, 3, ',', ''),
                        'period duration (sec)' => number_format((float) $period->getDuration() / 1000, 3, ',', ''),
                        'period memory (MB)'    => number_format((float) $period->getMemory() / (1024 * 1024), 0, ',', ''),
                    ];
                }
            }
        }

        return $data;
    }
}