<?php

namespace AppBundle\Benchmark;

use AppBundle\Entity;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Stopwatch\Stopwatch;

class Database
{
    const ITERATION_SIZE = 1000000;
    const CHUNK_SIZE     = 1000;
    const CLEAR_ON_FLUSH = true;

    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var Stopwatch
     */
    protected $stopwatch;

    /**
     * @var array
     */
    protected $options;

    /**
     * Iteration
     *
     * @var int
     */
    protected $iteration;

    /**
     * Chunk iteration
     *
     * @var int
     */
    protected $chunkIteration;

    /**
     * Database constructor.
     *
     * @param ObjectManager   $manager
     * @param OutputInterface $output
     * @param Stopwatch       $stopwatch
     * @param array           $options
     */
    public function __construct(ObjectManager $manager, OutputInterface $output, Stopwatch $stopwatch, array $options)
    {
        $this->manager   = $manager;
        $this->output    = $output;
        $this->stopwatch = $stopwatch;
        $this->options   = $this->resolveOptions($options);
    }

    /**
     * @param array $options
     *
     * @return array
     */
    protected function resolveOptions(array $options)
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'iterationSize' => self::ITERATION_SIZE,
            'chunkSize'     => self::CHUNK_SIZE,
            'clearOnFlush'  => self::CLEAR_ON_FLUSH,
        ]);

        return $resolver->resolve($options);
    }

    protected function handleFlush($identifier)
    {
        // (re-set) chunk iteration if a new iteration starts
        if ($this->iteration === 0) {
            $this->chunkIteration = 0;
        }

        // iteration limit reached, flush and stop
        if ($this->iteration === ($this->options['iterationSize'] - 1)) {
            $this->manager->flush();
            if ($this->options['clearOnFlush']) {
                $this->manager->clear();
            }
        } else {
            $this->chunkIteration++;
            if ($this->chunkIteration >= $this->options['chunkSize']) {
                $this->chunkIteration = 0;
                $this->manager->flush();
                if ($this->options['clearOnFlush']) {
                    $this->manager->clear();
                }
                $this->stopwatch->lap($identifier);
            }
        }
    }

    /**
     * @return $this
     */
    public function benchmarkAll()
    {
        $this
            ->benchmarkManyToOneUnidirectional()
            ->benchmarkOneToOneUnidirectional()
            ->benchmarkOneToOneBidirectional()
            ->benchmarkOneToOneSelfReferencing()
            ->benchmarkOneToManyBidirectional()
            ->benchmarkOneToManyUnidirectionalWithJoinTable()
            ->benchmarkOneToManySelfReferencing()
            ->benchmarkManyToManyUnidirectional()
            ->benchmarkManyToManyBidirectional()
            ->benchmarkManyToManySelfReferencing()
        ;

        return $this;
    }

    public function benchmarkManyToOneUnidirectional()
    {
        $identifier = 'many-to-one_unidirectional';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $address = new Entity\Address();
            $consumer = new Entity\Consumer();

            $consumer->setAddress($address);

            $this->manager->persist($address);
            $this->manager->persist($consumer);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkOneToOneUnidirectional()
    {
        $identifier = 'one-to-one_unidirectional';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $product  = new Entity\Product();
            $shipment = new Entity\Shipment();

            $product->setShipment($shipment);

            $this->manager->persist($shipment);
            $this->manager->persist($product);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkOneToOneBidirectional()
    {
        $identifier = 'one-to-one_bidirectional';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $customer = new Entity\Customer();
            $cart     = new Entity\Cart();

            $customer->setCart($cart);

            $this->manager->persist($customer);
            $this->manager->persist($cart);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkOneToOneSelfReferencing()
    {
        $identifier = 'one-to-one_self-referencing';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $studentOne   = new Entity\Student();
            $studentTwo   = new Entity\Student();
            $studentThree = new Entity\Student();

            $studentOne->setMentor($studentTwo);
            $studentTwo->setMentor($studentThree);

            $this->manager->persist($studentThree);
            $this->manager->persist($studentTwo);
            $this->manager->persist($studentOne);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkOneToManyBidirectional()
    {
        $identifier = 'one-to-many_bidirectional';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $application = new Entity\Application();
            $featureOne  = new Entity\Feature();
            $featureTwo  = new Entity\Feature();

            $application->addFeature($featureOne);
            $application->addFeature($featureTwo);

            $this->manager->persist($featureOne);
            $this->manager->persist($featureTwo);
            $this->manager->persist($application);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkOneToManyUnidirectionalWithJoinTable()
    {
        $identifier = 'one-to-many_unidirectional_with_join_table';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $employee = new Entity\Employee();
            $emailOne = new Entity\Email();
            $emailTwo = new Entity\Email();

            $employee->addEmail($emailOne);
            $employee->addEmail($emailTwo);

            $this->manager->persist($emailOne);
            $this->manager->persist($emailTwo);
            $this->manager->persist($employee);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkOneToManySelfReferencing()
    {
        $identifier = 'one-to-many_self-referencing';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $categoryOne   = new Entity\Category();
            $categoryTwo   = new Entity\Category();
            $categoryThree = new Entity\Category();
            $categoryFour  = new Entity\Category();

            $categoryOne->addChild($categoryTwo);
            $categoryOne->addChild($categoryThree);
            $categoryThree->addChild($categoryFour);

            $this->manager->persist($categoryFour);
            $this->manager->persist($categoryThree);
            $this->manager->persist($categoryTwo);
            $this->manager->persist($categoryOne);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkManyToManyUnidirectional()
    {
        $identifier = 'many-to-many_unidirectional';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $userOne = new Entity\User();
            $userTwo = new Entity\User();
            $roleOne = new Entity\Role();
            $roleTwo = new Entity\Role();

            $userOne->addRole($roleOne);
            $userTwo->addRole($roleOne);
            $userTwo->addRole($roleTwo);

            $this->manager->persist($roleOne);
            $this->manager->persist($roleTwo);
            $this->manager->persist($userOne);
            $this->manager->persist($userTwo);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkManyToManyBidirectional()
    {
        $identifier = 'many-to-many_bidirectional';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $articleOne = new Entity\Article();
            $articleTwo = new Entity\Article();
            $tagOne     = new Entity\Tag();
            $tagTwo     = new Entity\Tag();

            $articleOne->addTag($tagOne);
            $articleTwo->addTag($tagTwo);
            $tagTwo->addArticle($articleOne);

            $this->manager->persist($tagOne);
            $this->manager->persist($tagTwo);
            $this->manager->persist($articleOne);
            $this->manager->persist($articleTwo);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }

    public function benchmarkManyToManySelfReferencing()
    {
        $identifier = 'many-to-many_self-referencing';
        $this->stopwatch->start($identifier);

        for ($this->iteration = 0; $this->iteration < $this->options['iterationSize']; $this->iteration++) {
            $teenagerOne   = new Entity\Teenager();
            $teenagerTwo   = new Entity\Teenager();
            $teenagerThree = new Entity\Teenager();

            $teenagerOne->addMyFriend($teenagerTwo);
            $teenagerTwo->addMyFriend($teenagerThree);
            $teenagerThree->addMyFriend($teenagerOne);

            $this->manager->persist($teenagerThree);
            $this->manager->persist($teenagerTwo);
            $this->manager->persist($teenagerOne);

            $this->handleFlush($identifier);
        }

        $this->stopwatch->stop($identifier);

        return $this;
    }
}