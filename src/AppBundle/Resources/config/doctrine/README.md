## Many-To-One, Unidirectional

### Address, Consumer

## One-To-One, Unidirectional

### Product, Shipment

## One-To-One, Bidirectional

### Customer, Cart

## One-To-One, Self-referencing

### Student

## One-To-Many, Bidirectional

### Application, Feature

## One-To-Many, Unidirectional with Join Table

### Employee, Email

## One-To-Many, Self-referencing

### Category

## Many-To-Many, Unidirectional

### User, Role

## Many-To-Many, Bidirectional

### Article, Tag

## Many-To-Many, Self-referencing

### Teenager