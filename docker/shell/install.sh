#!/usr/bin/env bash

if [ -z "${BASE_DIR+x}" ]; then
    echo -e "\e[31mDO NOT CALL THIS SCRIPT DIRECTLY!\e[0m"
    exit 1
fi

cd $BASE_DIR

# set rights
docker exec -i "${PROJECT}_php-fpm_1" /bin/bash -c "chmod -R 777 /var/www/var"

# install dependencies
docker exec -i "${PROJECT}_php-fpm_1" /bin/bash -c "rm -rf /var/www/vendor && composer install --no-progress --no-interaction --optimize-autoloader --working-dir=/var/www"

# clear cache
docker exec -i "${PROJECT}_php-fpm_1" /bin/bash -c "php /var/www/bin/console cache:clear --no-warmup --env=benchmark"

# set rights again
docker exec -i "${PROJECT}_php-fpm_1" /bin/bash -c "chmod -R 777 /var/www/var"
