#!/usr/bin/env bash

if [ -z "${BASE_DIR+x}" ]; then
    echo -e "\e[31mDO NOT CALL THIS SCRIPT DIRECTLY!\e[0m"
    exit 1
fi

cd $BASE_DIR

# (re-)build docker containers
eval $DOCKER_COMPOSE stop -t0
docker volume ls -qf dangling=true | grep $PROJECT | xargs -r docker volume rm -f
eval $DOCKER_COMPOSE build --pull --force-rm
eval $DOCKER_COMPOSE up -d

# install composer globally
docker exec -i "${PROJECT}_php-fpm_1" /bin/bash -c "curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer"