#!/usr/bin/env bash

PARALLEL_TASKS=${1:-1}

export ITERATION_SIZE=5000
export CHUNK_SIZE=1000
export DATABASE_DRIVER=pdo_mysql

hosts=( mysql57 )
id_types=( bigint uuid uuid_binary uuid_binary_ordered_time )
for host in "${hosts[@]}"
do
    for id_type in "${id_types[@]}"
    do
        export DATABASE_HOST=${host}

        export ID_TYPE=${id_type}
        bin/console benchmark:database --skip-benchmark
        echo "run ${PARALLEL_TASKS} parallel tasks"
        for (( c=1; c<=$PARALLEL_TASKS; c++ ))
        do
            bin/console benchmark:database --skip-drop-schema --skip-create-schema --quiet &
        done
        wait
        echo "done"
    done
done